def fibonacci(n):
    if n <= 0:
        return "Input should be positive integer"
    elif n == 1 or n == 2:
        return 1
    else:
        a, b = 1, 1
        for _ in range(2, n):
            a, b = b, a + b
        return b


def test_fibonacci():
    assert fibonacci(7) == 13, "Test case failed!"


test_fibonacci()
