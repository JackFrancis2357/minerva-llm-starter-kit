from llama_cpp import Llama

# Testing with Mistral-7B-Instruct-v0.2-GGUF (https://huggingface.co/TheBloke/Mistral-7B-Instruct-v0.2-GGUF/blob/main/mistral-7b-instruct-v0.2.Q4_K_M.gguf)
llm = Llama(model_path="./models/mistral-7b-instruct-v0.2.Q4_K_M.gguf")
output = llm(
    "<s>[INST] Name the planets in the solar system. Please avoid any description about the planet and only provide their name [/INST]",
    max_tokens=200,  # Generate up to 200 tokens
)

print(output)

# Expected output (or close)
# {
#     "id": "cmpl-d07f289e-fdfc-46f5-bccb-e46ef353f161",
#     "object": "text_completion",
#     "created": 1706115388,
#     "model": "./models/mistral-7b-instruct-v0.2.Q4_K_M.gguf",
#     "choices": [
#         {
#             "text": " Mercury, Venus, Earth, Mars, Jupiter, Saturn, Uranus, Neptune.\n\nThese are the eight planets in our solar system, listed in order from the Sun.",
#             "index": 0,
#             "logprobs": None,
#             "finish_reason": "stop",
#         }
#     ],
#     "usage": {"prompt_tokens": 29, "completion_tokens": 43, "total_tokens": 72},
# }
