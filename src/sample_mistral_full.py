from llama_cpp import Llama

llm = Llama(model_path="./models/mistral-7b-instruct-v0.2.Q4_K_M.gguf", n_gpu_layers=-1, seed=4970, n_ctx=4096)
print("Question: ", end="")
question = input()
tokens = llm.tokenize(f"[INST]{question}/INST]".encode("utf-8"))
while True:
    answer = []
    parts = []
    for token in llm.generate(tokens, temp=0):
        parts.append(token)
        answer.append(token)
        if token == 2:  # EOS
            break
        try:
            part = llm.detokenize(parts).decode()
            print(part, end="", flush=True)
            parts = []
        except UnicodeDecodeError:
            pass
    tokens += answer
    print()
    print("Question: ", end="")
    question = input()
    if question == "exit":
        break
    tokens += llm.tokenize(f"[INST]{question}[/INST]".encode("utf-8"), add_bos=False)
    print(f"My current tokens are {tokens}")
