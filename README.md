# Minerva LLM Starter Kit

## Setup your environment

In a terminal, setup your virtual environment and install `llama-cpp-python` (the only dependency for the project).

Both of the commands below assume that the keyword `python` defaults to your

On Windows:

```shell
python -m venv .venv/
.venv/Scripts/activate
pip install llama-cpp-python
```

On Linux/Mac:

```shell
python -m venv .venv/
source .venv/bin/activate
pip install llama-cpp-python
```

I've also included a `requirements.txt` file if you want to use that instead.

For install on MacOS M1 you will want to use the following command to take advantage of Metal
`CMAKE_ARGS="-DLLAMA_METAL=on" pip install llama-cpp-python`

For other variants to leverage BLAS and other hardware accelerators see here: https://github.com/abetlen/llama-cpp-python

## Downloading LLM models from HuggingFace

The best resource for downloading models is HuggingFace user TheBloke: https://huggingface.co/TheBloke. For LLAMA-based models, you want to make sure you download models ending in `.gguf`. GGUF stands for GPT-Generated Unified Format, which is a quantization method. For personal laptop use, it's recommended to use a 7B model (OpenHermes 2.5 and Mistral Instruct are great options). Larger models are generally going to be either much slower or not fit into memory. For a current state of best models you can check out the HuggingFace open llm leaderboard: https://huggingface.co/spaces/HuggingFaceH4/open_llm_leaderboard. Some recommended models are:

Good General Purpose Models

- OpenHermes 2.5 Mistral 7B (https://huggingface.co/TheBloke/OpenHermes-2.5-Mistral-7B-GGUF)
- Mistral-7B-Instruct (https://huggingface.co/TheBloke/Mistral-7B-Instruct-v0.2-GGUF)
- LLaMa-2-7B-Chat (https://huggingface.co/TheBloke/Llama-2-7B-Chat-GGUF)

Code specific Models

- Magicoder 6.7B (https://huggingface.co/TheBloke/Magicoder-S-DS-6.7B-GGUF)
- CodeLlama 7B (https://huggingface.co/TheBloke/CodeLlama-7B-GGUF)
- WizardCoder Python 7B (https://huggingface.co/TheBloke/WizardCoder-Python-7B-V1.0-GGUF)

After downloading the model that you want, place it into the `./models` directory.

## Setting up Continue Code Copilot Integration

In VSCode, go to the extensions store and find `Continue - CodeLlama, GPT-4, and more`

![Alt text](image.png)

Install the extension.

Click on the extension in the left bar (the icon with the `>C D_` at the bottom in the screenshot below)

![Alt text](image-1.png)

Click the `+` at the bottom left, which should load the add new model page:

![Alt text](image-2.png)

Scroll down to llama.cpp and click it.

Instructions for setting up the server are provided here. When setting this up, I used CMAKE, so I had slightly different commands. YMMV based on which generator flavor you use. The commands I used with CMAKE are below. I did these in a separate folder to keep environments cleaner, but you should be able to do this in your current directory if you desire.

All of the commands below were done on a Windows machine. On Linux/MacOS, you should be able to replace steps 3-6 by just running `make`. Refer to the llama.cpp README if you run into issues: https://github.com/ggerganov/llama.cpp.

```shell
git clone https://github.com/ggerganov/llama.cpp
cd .\llama.cpp\
mkdir build
cd build
cmake ..
cmake --build . --config Release
```

At this point, you need to add your model to `.\llama.cpp\models`. Just copy and paste your model into this location.

Now, run the llama.cpp server from `.\llama.cpp\`:

```shell
.\build\bin\Release\server.exe -c 4096 --host 0.0.0.0 -t 16 --mlock -m models/codellama-7b-instruct.Q8_0.gguf
```

The Continue Extension should automatically identify the model and prompt you to add it to the config.json file. Accept that prompt. If that doesn't show up, you can find the config by opening the Continue Extension and then clicking the gear icon in the bottom right of the panel:

![Alt text](image-3.png)

Add your model to the `models` list. The entry in the list should look exactly like the screenshot below. The only change would be the model name if you are using something different. In addition, you may want to set `allowAnonymousTelemetry: false`. This is the only data Continue is capturing from you using the extension. With this turned off, Continue is able to be run in an air-gapped use case (https://continue.dev/docs/walkthroughs/running-continue-without-internet).

![Alt text](image-4.png)

Finally, make sure that the drop down in the bottom left of the Continue modal is set to your local model. In this case, mine would be set to mistral-7b-instruct.
